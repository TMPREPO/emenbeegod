package net.emenbee.egod;

import net.emenbee.egod.commands.Commandagod;
import net.emenbee.egod.commands.Commandgod;
import net.emenbee.egod.listeners.EventOnPlayerDamaged;
import net.emenbee.egod.listeners.EventOnPlayerJoin;
import net.emenbee.egod.listeners.EventOnPlayerQuit;
import net.emenbee.egod.listeners.EventPlayerTargetedByMonster;
import net.emenbee.egod.managers.*;
import net.emenbee.egod.repeating.tasks.CountdownTask;
import net.emenbee.egod.repeating.tasks.DisplayTask;
import net.emenbee.emenbeeplugin.egod.MultiThreadedEmenbeePlugin;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;

public class EmenbeeGod extends MultiThreadedEmenbeePlugin
{
    private static EmenbeeGod maininstance;
    private static ConfigManager configinstance;
    private static ChatManager chatinstance;
    private static PlayerDataManager playerdatainstance;
    private static GroupFileManager groupfileinstance;
    private static TimeFormatManager timeformatinstance;
    private static CountdownTask countdowntask;
    private static DisplayTask displaytask;
    public HashMap<UUID, Integer> immunitytimecountdown;
    public HashMap<UUID, Integer> cooldowntimecountdown;
    public HashMap<String, Integer> immunitygroups;
    public HashMap<String, Integer> cooldowngroups;
    public PluginManager pm = getServer().getPluginManager();
    public static Chat chat = null;

    public EmenbeeGod()
    {
        super(2);
    }

    @Override
    public void onEmenbeePluginLoad()
    {
        maininstance = this;
        configinstance = new ConfigManager();
        chatinstance = new ChatManager();
        playerdatainstance = new PlayerDataManager();
        groupfileinstance = new GroupFileManager();
        timeformatinstance = new TimeFormatManager();
        countdowntask = new CountdownTask();
        displaytask = new DisplayTask();
        immunitytimecountdown = new HashMap<UUID, Integer>();
        cooldowntimecountdown = new HashMap<UUID, Integer>();
    }

    @Override
    public void onEmenbeePluginEnable()
    {
        setupChat();

        if (!setupChat()) {
            log(SEVERE, "----------------------------------------------------------------------");
            log(SEVERE, "Failed to hook onto Vault, is the plugin installed? Disabling plugin. ");
            log(SEVERE, "----------------------------------------------------------------------");
            pm.disablePlugin(this);
        }

        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        getGroupFileManager().createGroupFile();
        getPlayerDataManager().createDataFile();
        pm.registerEvents(new EventOnPlayerJoin(), this);
        pm.registerEvents(new EventOnPlayerQuit(), this);
        pm.registerEvents(new EventOnPlayerDamaged(), this);
        pm.registerEvents(new EventPlayerTargetedByMonster(), this);
        getCountdownTask().runTaskTimerAsynchronously(this, 0, 20);
        getDisplayTask().runTaskTimerAsynchronously(this, 0, 20);
        getCommand("god").setExecutor(new Commandgod());
        getCommand("agod").setExecutor(new Commandagod());
        log(INFO, "Is now enabled.");
    }

    @Override
    public void onEmenbeePluginDisable()
    {
        if (!immunitytimecountdown.isEmpty()) {
            for (Map.Entry<UUID, Integer> immunityhm : immunitytimecountdown.entrySet()) {
                getPlayerDataManager().setCooldownTimeInPDF(Bukkit.getPlayer(immunityhm.getKey()), immunityhm.getValue());
            }
        }

        if (!cooldowntimecountdown.isEmpty()) {
            for (Map.Entry<UUID, Integer> cooldownhm : cooldowntimecountdown.entrySet()) {
                getPlayerDataManager().setCooldownTimeInPDF(Bukkit.getPlayer(cooldownhm.getKey()), cooldownhm.getValue());
            }
        }

        getScheduler().cancelTasks(this);
        immunitytimecountdown.clear();
        cooldowntimecountdown.clear();
        log(INFO, "Is now disabled.");
    }

    private boolean setupChat()
    {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

    public static EmenbeeGod getPlugin()
    {
        return maininstance;
    }

    public static ConfigManager getConfigManager()
    {
        if (configinstance == null) {
            configinstance = new ConfigManager();
        }

        return configinstance;
    }

    public static ChatManager getChatManager()
    {
        if (chatinstance == null) {
            chatinstance = new ChatManager();
        }

        return chatinstance;
    }

    public static PlayerDataManager getPlayerDataManager()
    {
        if (playerdatainstance == null) {
            playerdatainstance = new PlayerDataManager();
        }

        return playerdatainstance;
    }

    public static GroupFileManager getGroupFileManager()
    {
        if (groupfileinstance == null) {
            groupfileinstance = new GroupFileManager();
        }

        return groupfileinstance;
    }

    public static TimeFormatManager getTimeManager()
    {
        return timeformatinstance;
    }

    private static CountdownTask getCountdownTask()
    {
        if (countdowntask == null) {
            countdowntask = new CountdownTask();
        }

        return countdowntask;
    }

    private static DisplayTask getDisplayTask()
    {
        if (displaytask == null) {
            displaytask = new DisplayTask();
        }

        return displaytask;
    }
}