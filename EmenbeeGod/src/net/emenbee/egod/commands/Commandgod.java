package net.emenbee.egod.commands;

import net.emenbee.egod.EmenbeeGod;
import net.emenbee.egod.managers.ChatManager;
import net.emenbee.egod.managers.ConfigManager;
import net.emenbee.egod.managers.PlayerDataManager;
import net.emenbee.egod.managers.TimeFormatManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static java.util.logging.Level.INFO;

public class Commandgod implements CommandExecutor
{
    private EmenbeeGod getPlugin = EmenbeeGod.getPlugin();
    private ChatManager getChatManager = EmenbeeGod.getChatManager();
    private ConfigManager getCfg = EmenbeeGod.getConfigManager();
    private PlayerDataManager PDManager = EmenbeeGod.getPlayerDataManager();
    private TimeFormatManager getTManager = EmenbeeGod.getTimeManager();

    public boolean onCommand(CommandSender s, Command cmd, String str, String[] args)
    {
        if (!cmd.getName().equalsIgnoreCase("god")) {
            return true;
        }

        if (!(s instanceof Player)) {
            getPlugin.log(INFO, "You're already a god, Admin!");
        }

        Player p = (Player) s;

        if (!p.hasPermission("egod.god")) {
            getChatManager.sendMessage(p, getCfg.getNoPermissionMessage());
            return true;
        }

        if (args.length == 0) {
            for (String helpmessage : getCfg.getEmenbeeGodPlayerHelpListMessage()) {
                getChatManager.sendMessage(p, helpmessage);
            }
            return true;
        }

        if (args[0].equalsIgnoreCase("timecheck")) {
            for (String timecheckmessage : getCfg.getTimeCheckListMessage()) {
                getChatManager.sendMessage(p, timecheckmessage
                        .replace("{godhours}", String.valueOf(getTManager.getHours(PDManager.getImmunityTime(p))))
                        .replace("{godminutes}", String.valueOf(getTManager.getMinutes(PDManager.getImmunityTime(p))))
                        .replace("{godseconds}", String.valueOf(getTManager.getSeconds(PDManager.getImmunityTime(p))))
                        .replace("{cooldownhours}", String.valueOf(getTManager.getHours(PDManager.getCooldownTime(p))))
                        .replace("{cooldownminutes}", String.valueOf(getTManager.getMinutes(PDManager.getCooldownTime(p))))
                        .replace("{cooldownseconds}", String.valueOf(getTManager.getSeconds(PDManager.getCooldownTime(p)))));
            }
            return true;
        }

        if (args[0].equalsIgnoreCase("toggledisplay")) {
            PDManager.toggleDisplay(p.getUniqueId());

            if (PDManager.getDisplayTimeBoolean(p)) {
                getChatManager.sendMessage(p, getCfg.getToggleDisplayMessage().replace("{mode}", "enabled"));
            } else {
                getChatManager.sendMessage(p, getCfg.getToggleDisplayMessage().replace("{mode}", "disabled"));
            }
            return true;
        }


        if (args[0].equalsIgnoreCase("disable")) {
            if (PDManager.ImmunityContainsPlayer(p)) {
                PDManager.setImmunityTimeInPDF(p, PDManager.getImmunityTimeFromHashMap(p.getUniqueId()));
                getPlugin.log(INFO, "disable command executed " + PDManager.getImmunityTimeFromHashMap(p.getUniqueId()));
                PDManager.removePlayerFromImmunityHM(p);
                getChatManager.sendMessage(p, getCfg.getGodDisabledMessage());
            } else {
                getChatManager.sendMessage(p, getCfg.getGodNotEnabledMessage());
            }
            return true;
        }

        if (args[0].equalsIgnoreCase("enable")) {
            if (PDManager.CooldownContainsPlayer(p) || PDManager.getImmunityTime(p) == 0) {
                getChatManager.sendMessage(p, getCfg.getNoImmunityTimeLeftMessage());
                return true;
            }

            if (!PDManager.ImmunityContainsPlayer(p)) {
                PDManager.addPlayerToImmunityHM(p);
                getChatManager.sendMessage(p, getCfg.getGodEnabledMessage());
            }
            return true;
        }
        return true;
    }
}