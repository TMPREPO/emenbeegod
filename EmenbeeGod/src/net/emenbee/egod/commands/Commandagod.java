package net.emenbee.egod.commands;

import net.emenbee.egod.EmenbeeGod;
import net.emenbee.egod.managers.ChatManager;
import net.emenbee.egod.managers.ConfigManager;
import net.emenbee.egod.managers.PlayerDataManager;
import net.emenbee.egod.managers.TimeFormatManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commandagod implements CommandExecutor
{
    private EmenbeeGod getPlugin = EmenbeeGod.getPlugin();
    private ConfigManager getCfg = EmenbeeGod.getConfigManager();
    private ChatManager getChatManager = EmenbeeGod.getChatManager();
    private PlayerDataManager PDManager = EmenbeeGod.getPlayerDataManager();
    private TimeFormatManager getTManager = EmenbeeGod.getTimeManager();

    public boolean onCommand(CommandSender s, Command cmd, String str, String[] args)
    {
        int time = 0;

        if (!cmd.getName().equalsIgnoreCase("agod")) {
            return true;
        }

        if (!s.hasPermission("egod.admin")) {
            getChatManager.sendMessage(s, getCfg.getNoPermissionMessage());
            return true;
        }

        if (args.length == 0) {
            for (String helpmessage : getCfg.getEmenbeeGodAdminHelpListMessage()) {
                getChatManager.sendMessage(s, helpmessage);
            }
            return true;
        }

        if (args[0].equalsIgnoreCase("settime")) {
            if (args.length == 0 || args.length != 3) {
                getChatManager.sendMessage(s, getCfg.getNotEnoughArgsMessage());
                return true;
            }

            Player target = Bukkit.getPlayerExact(args[1]);

            if (!(target instanceof Player)) {
                getChatManager.sendMessage(s, getCfg.getTargetNotAPlayerMessage());
                return true;
            }

            try {
                time = Integer.valueOf(args[2]);
            } catch (NumberFormatException Args2NotANumber) {
                getChatManager.sendMessage(s, getCfg.getArgsNotAnIntMessage());
                return true;
            }

            if (args[3].equalsIgnoreCase("cooldown")) {

                if (PDManager.ImmunityContainsPlayer(target)) {
                    PDManager.setImmunityTimeInPDF(target, PDManager.getImmunityTimeFromHashMap(target.getUniqueId()));
                    PDManager.removePlayerFromImmunityHM(target);
                }

                PDManager.setCooldownTime(target, time);
                getChatManager.sendMessage(s, getCfg.getSetPlayersTimeTo()
                        .replace("{target}", target.getName())
                        .replace("{timetype}", "cooldown")
                        .replace("{time}", getCfg.getTimeSetToAlias()
                                .replace("{hours}", String.valueOf(getTManager.getHours(time)))
                                .replace("{minutes}", String.valueOf(getTManager.getMinutes(time)))
                                .replace("{seconds}", String.valueOf(getTManager.getSeconds(time)))));
                return true;
            }

            if (args[3].equalsIgnoreCase("immunity")) {
                PDManager.setImmunityTime(target, time);
                getChatManager.sendMessage(s, getCfg.getSetPlayersTimeTo()
                        .replace("{target}", target.getName())
                        .replace("{timetype}", "immunity")
                        .replace("{time}", getCfg.getTimeSetToAlias()
                                .replace("{hours}", String.valueOf(getTManager.getHours(time)))
                                .replace("{minutes}", String.valueOf(getTManager.getMinutes(time)))
                                .replace("{seconds}", String.valueOf(getTManager.getSeconds(time)))));
            }
            return true;
        }
        return true;
    }
}