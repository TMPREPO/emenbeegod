package net.emenbee.egod.managers;

import net.emenbee.egod.EmenbeeGod;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ChatManager
{
    private ConfigManager getCfg = EmenbeeGod.getConfigManager();

    public void sendMessage(CommandSender sender, String message)
    {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message.replace("{prefix}", getCfg.getPrefix())));
    }

    public void sendMessage(Player player, String message)
    {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', message.replace("{prefix}", getCfg.getPrefix())));
    }

    public void sendJSONMessage(Player player, String message)
    {
        CraftPlayer p = (CraftPlayer)player;
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + ChatColor.translateAlternateColorCodes('&', message.replace("{prefix}", getCfg.getPrefix())) + "\"}");
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte)2);
        p.getHandle().playerConnection.sendPacket(ppoc);
    }
}
