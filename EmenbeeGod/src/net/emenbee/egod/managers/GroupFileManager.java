package net.emenbee.egod.managers;

import net.emenbee.egod.EmenbeeGod;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;

public class GroupFileManager
{
    private EmenbeeGod getPlugin = EmenbeeGod.getPlugin();
    private static File groupf;
    private static YamlConfiguration groupy;

    public void createGroupFile()
    {
        groupf = new File(getPlugin.getDataFolder() + File.separator + "groups.yml");
        groupy = YamlConfiguration.loadConfiguration(groupf);

        boolean save = false;

        if (!groupf.exists()) {
            try {
                for (String groups : getPlugin.chat.getGroups()) {
                    groupy.set("Groups." + groups + "Permission", "egod." + groups);
                    groupy.set("Groups." + groups + ".Immunity-Time", 300);
                    groupy.set("Groups." + groups + ".Cooldown-Time", 0);
                    getPlugin.log(INFO, "Added group " + groups + " to group.yml.");
                    save = true;
                }
            } catch (NullPointerException permissionsPluginNotFound) {
                getPlugin.log(INFO, "Permission plugin not found/supported. Please contact TheMasteredPanda for more information on the matter.");
                return;
            }
            save = true;
        }

        if (save) {
            try {
                groupy.save(groupf);
                getPlugin.log(INFO, "Created groups.yml");
            } catch (IOException couldNotCreateFile) {
                getPlugin.log(SEVERE, "Could not create groups.yml, caused by:");
                couldNotCreateFile.printStackTrace();
            }
        }
    }

    public static YamlConfiguration getGroupF()
    {
        return groupy;
    }

    public void saveGroupF()
    {
        try {
            if (groupy == null) {
                groupy = YamlConfiguration.loadConfiguration(groupf);
            }
            groupy.save(groupf);
        } catch (IOException couldNotSaveFile) {
            getPlugin.log(SEVERE, "Failed to save groups.yml, caused by:");
            couldNotSaveFile.printStackTrace();
        }
    }
}
