package net.emenbee.egod.managers;

public class TimeFormatManager
{

    public int getHours(int numberOfSeconds)
    {
        int hours = numberOfSeconds / 3600;

        return hours;
    }

    public int getMinutes(int numberOfSeconds)
    {
        int minutes = (numberOfSeconds % 3600) / 60;

        return minutes;
    }

    public int getSeconds(int numberOfSeconds)
    {
        int seconds = numberOfSeconds % 60;

        return seconds;
    }
}
