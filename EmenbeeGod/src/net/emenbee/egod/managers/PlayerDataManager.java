package net.emenbee.egod.managers;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import net.emenbee.egod.EmenbeeGod;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.WARNING;

public class PlayerDataManager
{
    private static EmenbeeGod getPlugin = EmenbeeGod.getPlugin();
    private GroupFileManager getGroupsF = EmenbeeGod.getGroupFileManager();
    private static File datafile;
    private File playerfile;
    private YamlConfiguration playerfileyaml;


    public void createDataFile()
    {
        datafile = new File(getPlugin.getDataFolder() + File.separator + "data");

        if (!datafile.exists()) {
            datafile.mkdir();
        }
    }

    public void createPlayerFile(final UUID uuid)
    {

        getPlugin.runOnThreadPool(new Runnable()
        {
            public void run()
            {
                if (!datafile.exists()) {
                    getPlugin.log(INFO, "No data folder found, creating one");
                    createDataFile();
                }

                playerfile = new File(getPlugin.getDataFolder() + File.separator + "data" + File.separator + uuid.toString() + ".yml");
                playerfileyaml = YamlConfiguration.loadConfiguration(playerfile);

                boolean save = false;
                if (!playerfile.exists()) {
                    playerfileyaml.set("ImmunityTimeLeft", getImmunityTimeFromGF(Bukkit.getPlayer(uuid)));
                    playerfileyaml.set("CooldownTimeLeft", 0);
                    playerfileyaml.set("DisplayTime", true);
                    save = true;
                }

                if (save) {
                    try {
                        playerfileyaml.save(playerfile);
                        getPlugin.log(INFO, "Created file " + uuid.toString() + "/" + Bukkit.getPlayer(uuid).getName());
                    } catch (IOException failedToCreatePlayerFile) {
                        getPlugin.log(WARNING, "Failed to create player file, caused by:");
                        failedToCreatePlayerFile.printStackTrace();
                    }
                }
            }
        });
    }

    private void savePlayerFile(File playerDataFile, YamlConfiguration playerYamlFile)
    {
        try {
            playerYamlFile.save(playerDataFile);
        } catch (IOException couldNotSavePlayerFile) {
            couldNotSavePlayerFile.printStackTrace();
        }
    }

    public int getImmunityTime(Player player)
    {
        if (player.isOnline() && getPlugin.immunitytimecountdown.containsKey(player.getUniqueId())) {
            return getPlugin.immunitytimecountdown.get(player.getUniqueId());
        } else {
            File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

            if (!pdfile.exists()) {
                getPlugin.log(WARNING, "Could not find player file. getImmunityTime()");
                return 0;
            }

            YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

            return pdyaml.getInt("ImmunityTimeLeft");
        }
    }

    public int getImmunityTimeFromPDF(UUID playeruuid)
    {
        File pdfile = new File(datafile.getAbsolutePath() + File.separator + playeruuid.toString() + ".yml");

        if (!pdfile.exists()) {
            getPlugin.log(WARNING, "Could not find player file. getImmunityTimeFromPDF()");
            return 0;
        }

        YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

        return pdyaml.getInt("ImmunityTimeLeft");
    }

    public int getImmunityTimeFromHashMap(UUID playeruuid)
    {
        return getPlugin.immunitytimecountdown.get(playeruuid);
    }

    public void setImmunityTime(Player player, int numberOfSeconds)
    {
        if (player.isOnline()) {
            if (getPlugin.immunitytimecountdown.containsKey(player.getUniqueId())) {
                getPlugin.immunitytimecountdown.put(player.getUniqueId(), numberOfSeconds);
            }
        } else {
            File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

            if (!pdfile.exists()) {
                getPlugin.log(WARNING, "Could not find player file. setImmunityTime()");
                return;
            }

            YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

            pdyaml.set("ImmunityTimeLeft", numberOfSeconds);


        }
    }

    public int getCooldownTime(Player player)
    {
        if (player.isOnline() && getPlugin.cooldowntimecountdown.containsKey(player.getUniqueId())) {
            return getPlugin.cooldowntimecountdown.get(player.getUniqueId());
        } else {
            File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

            if (!pdfile.exists()) {
                getPlugin.log(WARNING, "Could not find player file. getCooldownTime()");
                return 0;
            }

            YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

           return pdyaml.getInt("CooldownTimeLeft");
        }
    }

    public int getCooldownTimeFromPDF(UUID playeruuid)
    {
        File pdfile = new File(datafile.getAbsolutePath() + File.separator + playeruuid.toString() + ".yml");

        if (!pdfile.exists()) {
            getPlugin.log(WARNING, "Could not find player file. getCooldownTimeFromPDF()");
            return 0;
        }

        YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);
        getPlugin.log(INFO, "Number of seconds being put into the hashmap: " + pdyaml.getInt("CooldownTimeLeft"));
        return pdyaml.getInt("CooldownTimeLeft");
    }

    public int getCooldownTimeFromHashMap(UUID playeruuid)
    {
        return getPlugin.cooldowntimecountdown.get(playeruuid);
    }

    public void setCooldownTime(Player player, int numberOfSeconds)
    {
        if (player.isOnline()) {
            getPlugin.cooldowntimecountdown.put(player.getUniqueId(), numberOfSeconds);
        } else {
            File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

            if (!pdfile.exists()) {
                getPlugin.log(WARNING, "Could not find player file. setCooldownTime()");
            }

            YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

            pdyaml.set("CooldownTimeLeft", numberOfSeconds);
            savePlayerFile(pdfile, pdyaml);
        }
    }

    public int getImmunityTimeFromGF(Player player)
    {
        List<String> groupList = new ArrayList<String>(getGroupsF.getGroupF().getConfigurationSection("Groups").getKeys(false));
        Multiset<String> groups = HashMultiset.create(groupList);


        for (Multiset.Entry<String> entry : groups.entrySet()) {
            if (player.hasPermission("Groups." + entry.getElement() + ".Permission")) {
                return getGroupsF.getGroupF().getInt("Groups." + entry.getElement() + ".Immunity-Time");
            }
        }
        return 0;
    }

    public int getCooldownTimeFromGF(Player player)
    {
        List<String> groupList = new ArrayList<String>(getGroupsF.getGroupF().getConfigurationSection("Groups").getKeys(false));
        Multiset<String> groups = HashMultiset.create(groupList);


        for (Multiset.Entry<String> entry : groups.entrySet()) {
            if (player.hasPermission("Groups." + entry.getElement() + ".Permission")) {
                return getGroupsF.getGroupF().getInt("Groups." + entry.getElement() + ".Cooldown-Time");
            }
        }
        return 0;
    }


    public boolean isPlayerFileCreated(Player player)
    {
        File pdfile = new File(datafile.getAbsolutePath() + player.getUniqueId().toString() + ".yml");

        if (pdfile.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean getDisplayTimeBoolean(Player player)
    {
        File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

        if (!pdfile.exists()) {
            getPlugin.log(WARNING, "Could not find player file. getDisplayTimeBoolean()");
            return true;
        }

        YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

        return pdyaml.getBoolean("DisplayTime");
    }

    public void addPlayerToImmunityHM(Player player)
    {
        File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

        if (!pdfile.exists()) {
            getPlugin.log(WARNING, "Could not find player file.");
            return;
        }

        YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

        getPlugin.immunitytimecountdown.put(player.getUniqueId(), pdyaml.getInt("ImmunityTimeLeft"));
    }

    public void addPlayerToCooldownHM(Player player)
    {
        File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

        if (!pdfile.exists()) {
            getPlugin.log(WARNING, "Could not find player file. addPlayerToCooldownHM()");
            return;
        }

        YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

        getPlugin.cooldowntimecountdown.put(player.getUniqueId(), pdyaml.getInt("CooldownTimeLeft"));
    }

    public void removePlayerFromImmunityHM(Player player)
    {
        File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

        if (!pdfile.exists()) {
            getPlugin.log(WARNING, "Could not find player file. removePlayerFromImmunityHM()");
            return;
        }

        YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

        pdyaml.set("ImmunityTimeLeft", getPlugin.immunitytimecountdown.get(player.getUniqueId()));
        getPlugin.immunitytimecountdown.remove(player.getUniqueId());
    }

    public void removePlayerFromCooldownHM(Player player)
    {
        File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

        if (!pdfile.exists()) {
            getPlugin.log(WARNING, "Could not find player file. removePlayerFromCooldownHM()");
            return;
        }

        YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

        pdyaml.set("CooldownTimeLeft", getPlugin.cooldowntimecountdown.get(player.getUniqueId()));
        getPlugin.cooldowntimecountdown.remove(player.getUniqueId());
    }

    public boolean ImmunityContainsPlayer(Player player)
    {
        if (getPlugin.immunitytimecountdown.containsKey(player.getUniqueId())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean CooldownContainsPlayer(Player player)
    {
        if (getPlugin.cooldowntimecountdown.containsKey(player.getUniqueId())) {
            return true;
        } else {
            return false;
        }
    }

    public void setImmunityTimeInPDF(Player player, int numberOfSeconds)
    {
        File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

        if (!pdfile.exists()) {
            getPlugin.log(WARNING, "Could not find player file. setImmunityTimeInPDF()");
            return;
        }

        YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

        pdyaml.set("ImmunityTimeLeft", numberOfSeconds);
        savePlayerFile(pdfile, pdyaml);
    }

    public void setCooldownTimeInPDF(Player player, int numberOfSeconds)
    {
        File pdfile = new File(datafile.getAbsolutePath() + File.separator + player.getUniqueId().toString() + ".yml");

        if (!pdfile.exists()) {
            getPlugin.log(WARNING, "Could not find player file. setCooldownTimeInPDF()");
            return;
        }

        YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);
        getPlugin.log(INFO, "Number of seconds being saved: " + numberOfSeconds);
        pdyaml.set("CooldownTimeLeft", numberOfSeconds);
        savePlayerFile(pdfile, pdyaml);
    }

    public void toggleDisplay(UUID playeruuid)
    {
        File pdfile = new File(datafile.getAbsolutePath() + File.separator + playeruuid.toString() + ".yml");

        if (!pdfile.exists()) {
            getPlugin.log(WARNING, "Could not find player file. toggleDisplay()");
            return;
        }

        YamlConfiguration pdyaml = YamlConfiguration.loadConfiguration(pdfile);

        if (pdyaml.getBoolean("DisplayTime")) {
            pdyaml.set("DisplayTime", false);
            savePlayerFile(pdfile, pdyaml);
        } else {
            pdyaml.set("DisplayTime", true);
            savePlayerFile(pdfile, pdyaml);
        }
    }
}