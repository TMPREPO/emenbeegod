package net.emenbee.egod.managers;

import net.emenbee.egod.EmenbeeGod;

import java.util.List;

public class ConfigManager
{
    private EmenbeeGod getPlugin = EmenbeeGod.getPlugin();

    public String getPrefix()
    {
        return getPlugin.getConfig().getString("Messages.Prefix");
    }

    public String getNoPermissionMessage()
    {
        return getPlugin.getConfig().getString("Messages.NoPermission");
    }

    public String getNoImmunityTimeLeftMessage()
    {
        return getPlugin.getConfig().getString("Messages.Player.NoImmunityTimeLeft");
    }

    public String getGodEnabledMessage()
    {
        return getPlugin.getConfig().getString("Messages.Player.GodEnabled");
    }

    public String getGodDisabledMessage()
    {
        return getPlugin.getConfig().getString("Messages.Player.GodDisabled");
    }

    public String getImmunityTimeActionBarMessage()
    {
        return getPlugin.getConfig().getString("ImmunityTimeActionBarMsg");
    }

    public String getCooldownTimeActionBarMessage()
    {
        return getPlugin.getConfig().getString("CooldownTimeActionBarMsg");
    }

    public List<String> getTimeCheckListMessage()
    {
        return getPlugin.getConfig().getStringList("Messages.Player.TimeCheck");
    }

    public List<String> getEmenbeeGodPlayerHelpListMessage()
    {
        return getPlugin.getConfig().getStringList("Messages.Player.HelpList");
    }

    public String getTargetNotAPlayerMessage()
    {
        return getPlugin.getConfig().getString("Messages.Player.TargetNotAPlayer");
    }

    public List<String> getEmenbeeGodAdminHelpListMessage()
    {
        return getPlugin.getConfig().getStringList("Messages.Admin.HelpList");
    }

    public String getNotEnoughArgsMessage()
    {
        return getPlugin.getConfig().getString("Messages.NotEnoughArgs");
    }

    public String getArgsNotAnIntMessage()
    {
        return getPlugin.getConfig().getString("Messages.ArgsNotAnInt");
    }

    public String getSetPlayersTimeTo()
    {
        return getPlugin.getConfig().getString("Messages.Admin.TimeSetTo");
    }
    public String getGodNotEnabledMessage()
    {
        return getPlugin.getConfig().getString("Messages.Player.GodNotEnabled");
    }

    public String getToggleDisplayMessage()
    {
        return getPlugin.getConfig().getString("Messages.Player.ToggleDisplay");
    }

    public String getTimeSetToAlias()
    {
        return getPlugin.getConfig().getString("Messages.Admin.TimeAlias");
    }

    public String getCooldownEndedMessage()
    {
        return getPlugin.getConfig().getString("Messages.Player.CooldownEnded");
    }
}