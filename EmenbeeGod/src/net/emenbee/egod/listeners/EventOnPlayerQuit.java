package net.emenbee.egod.listeners;

import net.emenbee.egod.EmenbeeGod;
import net.emenbee.egod.managers.PlayerDataManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import static java.util.logging.Level.INFO;

public class EventOnPlayerQuit implements Listener
{
    private EmenbeeGod getPlugin = EmenbeeGod.getPlugin();
    private PlayerDataManager PDManager = EmenbeeGod.getPlayerDataManager();

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e)
    {
        Player p = e.getPlayer();

        if (PDManager.ImmunityContainsPlayer(p)) {
            PDManager.setImmunityTimeInPDF(p, PDManager.getImmunityTimeFromHashMap(p.getUniqueId()));
            PDManager.removePlayerFromImmunityHM(p);
            getPlugin.log(INFO, "onQuit triggers ImmunityContainsPLayer");
        }

        if (PDManager.CooldownContainsPlayer(p)) {
            PDManager.setCooldownTimeInPDF(p, PDManager.getCooldownTimeFromHashMap(p.getUniqueId()));
            PDManager.removePlayerFromCooldownHM(p);
            getPlugin.log(INFO, "onQuit triggers CooldownContainsPlayer");
        }
    }
}
