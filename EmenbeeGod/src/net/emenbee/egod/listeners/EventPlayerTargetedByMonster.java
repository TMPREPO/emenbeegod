package net.emenbee.egod.listeners;

import net.emenbee.egod.EmenbeeGod;
import net.emenbee.egod.managers.PlayerDataManager;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTargetEvent;

public class EventPlayerTargetedByMonster implements Listener
{
    private EmenbeeGod getPlugin = EmenbeeGod.getPlugin();
    private PlayerDataManager PDManager = EmenbeeGod.getPlayerDataManager();

    @EventHandler
    public void onPlayerTargetedByMonster(EntityTargetEvent e)
    {
        if (!(e.getTarget() instanceof Player)) {
            return;
        }

        Player p = (Player)e.getTarget();

        if (!PDManager.ImmunityContainsPlayer(p)) {
            return;
        }

        if (!(e.getEntity() instanceof Monster)) {
            return;
        }

        e.setTarget(null);
        e.setCancelled(true);
    }
}
