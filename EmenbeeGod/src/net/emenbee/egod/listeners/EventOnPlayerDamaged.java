package net.emenbee.egod.listeners;

import net.emenbee.egod.EmenbeeGod;
import net.emenbee.egod.managers.PlayerDataManager;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EventOnPlayerDamaged implements Listener
{
    private EmenbeeGod getPlugin = EmenbeeGod.getPlugin();
    private PlayerDataManager PDManager = EmenbeeGod.getPlayerDataManager();

    @EventHandler
    public void onPlayerDamageByEntity(EntityDamageByEntityEvent e)
    {
        if (!(e.getEntity() instanceof Player)) {
            return;
        }

        Player p = (Player)e.getEntity();

        if (!PDManager.ImmunityContainsPlayer(p)) {
            return;
        }

        if (!(e.getDamager() instanceof Monster)) {
            return;
        }

        e.setCancelled(true);
    }
}
