package net.emenbee.egod.listeners;

import net.emenbee.egod.EmenbeeGod;
import net.emenbee.egod.managers.PlayerDataManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static java.util.logging.Level.INFO;

public class EventOnPlayerJoin implements Listener
{
    private EmenbeeGod getPlugin = EmenbeeGod.getPlugin();
    private PlayerDataManager PDManager = EmenbeeGod.getPlayerDataManager();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e)
    {
        Player p = e.getPlayer();

        if (!PDManager.isPlayerFileCreated(p)) {
            PDManager.createPlayerFile(p.getUniqueId());
        }

        while (PDManager.isPlayerFileCreated(p)) {
            if (PDManager.getCooldownTimeFromPDF(p.getUniqueId()) != 0) {
                getPlugin.log(INFO, "getCooldownTimeFromPDF on Join is fired.");
                PDManager.addPlayerToCooldownHM(p);
            }
        }
    }
}
