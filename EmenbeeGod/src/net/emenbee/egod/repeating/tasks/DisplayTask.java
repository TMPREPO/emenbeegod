package net.emenbee.egod.repeating.tasks;

import net.emenbee.egod.EmenbeeGod;
import net.emenbee.egod.managers.ChatManager;
import net.emenbee.egod.managers.ConfigManager;
import net.emenbee.egod.managers.PlayerDataManager;
import net.emenbee.egod.managers.TimeFormatManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;
import java.util.UUID;

public class DisplayTask extends BukkitRunnable
{
    private EmenbeeGod getPlugin = EmenbeeGod.getPlugin();
    private PlayerDataManager PDManager = EmenbeeGod.getPlayerDataManager();
    private ChatManager getChatManager = EmenbeeGod.getChatManager();
    private ConfigManager getCfg = EmenbeeGod.getConfigManager();
    private TimeFormatManager getTManager = EmenbeeGod.getTimeManager();

    private void displayImmunityTime()
    {
        for (Map.Entry<UUID, Integer> immunityhm : getPlugin.immunitytimecountdown.entrySet()) {
            Player p = Bukkit.getPlayer(immunityhm.getKey());

            if (!PDManager.getDisplayTimeBoolean(p)) {
                return;
            }

            getChatManager.sendJSONMessage(p, getCfg.getImmunityTimeActionBarMessage()
            .replace("{hours}", String.valueOf(getTManager.getHours(immunityhm.getValue())))
            .replace("{minutes}", String.valueOf(getTManager.getMinutes(immunityhm.getValue())))
            .replace("{seconds}", String.valueOf(getTManager.getSeconds(immunityhm.getValue()))));
        }
    }

    private void displayCooldownTime()
    {
        for (Map.Entry<UUID, Integer> cooldownhm : getPlugin.cooldowntimecountdown.entrySet()) {
            Player p = Bukkit.getPlayer(cooldownhm.getKey());

            if (!PDManager.getDisplayTimeBoolean(p)) {
                return;
            }

            getChatManager.sendJSONMessage(p, getCfg.getCooldownTimeActionBarMessage()
                    .replace("{hours}", String.valueOf(getTManager.getHours(cooldownhm.getValue())))
                    .replace("{minutes}", String.valueOf(getTManager.getMinutes(cooldownhm.getValue())))
                    .replace("{seconds}", String.valueOf(getTManager.getSeconds(cooldownhm.getValue()))));
        }
    }

    public void run()
    {
        displayImmunityTime();
        displayCooldownTime();
    }
}
