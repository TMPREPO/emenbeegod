package net.emenbee.egod.repeating.tasks;

import net.emenbee.egod.EmenbeeGod;
import net.emenbee.egod.managers.ChatManager;
import net.emenbee.egod.managers.ConfigManager;
import net.emenbee.egod.managers.PlayerDataManager;
import net.emenbee.egod.managers.TimeFormatManager;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;
import java.util.UUID;

public class CountdownTask extends BukkitRunnable
{
    private EmenbeeGod getPlugin = EmenbeeGod.getPlugin();
    private PlayerDataManager PDManager = EmenbeeGod.getPlayerDataManager();
    private ChatManager getChatManager = EmenbeeGod.getChatManager();
    private ConfigManager getCfg = EmenbeeGod.getConfigManager();
    private TimeFormatManager getTManager = EmenbeeGod.getTimeManager();

    public void run()
    {
        for (Map.Entry<UUID, Integer> immuntiyhm : getPlugin.immunitytimecountdown.entrySet()) {

            if (Bukkit.getPlayer(immuntiyhm.getKey()).isOnline()) {
                immuntiyhm.setValue(immuntiyhm.getValue() - 1);
            }

            if (immuntiyhm.getValue() == 0) {
                PDManager.setImmunityTimeInPDF(Bukkit.getPlayer(immuntiyhm.getKey()), 0);

                if (PDManager.getCooldownTimeFromPDF(immuntiyhm.getKey()) == 0) {
                    PDManager.setCooldownTimeInPDF(Bukkit.getPlayer(immuntiyhm.getKey()), PDManager.getCooldownTimeFromGF(Bukkit.getPlayer(immuntiyhm.getKey())));
                }

                getChatManager.sendMessage(Bukkit.getPlayer(immuntiyhm.getKey()), getCfg.getNoImmunityTimeLeftMessage());
                PDManager.addPlayerToCooldownHM(Bukkit.getPlayer(immuntiyhm.getKey()));
                PDManager.removePlayerFromImmunityHM(Bukkit.getPlayer(immuntiyhm.getKey()));
            }
        }

        for (Map.Entry<UUID, Integer> cooldownhm : getPlugin.cooldowntimecountdown.entrySet()) {

            if (Bukkit.getPlayer(cooldownhm.getKey()).isOnline()) {
                cooldownhm.setValue(cooldownhm.getValue() - 1);
            }

            if (cooldownhm.getValue() == 0) {
                PDManager.setCooldownTimeInPDF(Bukkit.getPlayer(cooldownhm.getKey()), 0);

                if (PDManager.getImmunityTimeFromPDF(cooldownhm.getKey()) == 0) {
                    PDManager.setImmunityTimeInPDF(Bukkit.getPlayer(cooldownhm.getKey()), PDManager.getImmunityTimeFromGF(Bukkit.getPlayer(cooldownhm.getKey())));
                }

                getChatManager.sendMessage(Bukkit.getPlayer(cooldownhm.getKey()), getCfg.getCooldownEndedMessage());
                PDManager.removePlayerFromCooldownHM(Bukkit.getPlayer(cooldownhm.getKey()));
            }
        }
    }
}