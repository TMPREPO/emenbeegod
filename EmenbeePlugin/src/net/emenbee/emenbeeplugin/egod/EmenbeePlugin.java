package net.emenbee.emenbeeplugin.egod;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.logging.Level;
import java.util.logging.Logger;

public class EmenbeePlugin extends JavaPlugin
{
    public Logger logger = getLogger();

    public final void onLoad()
    {
        onEmenbeePluginLoad();
    }


    public final void onEnable()
    {
        onEmenbeePluginEnable();
    }

    public final void onDisable()
    {
        onEmenbeePluginDisable();
    }

    public BukkitScheduler getScheduler()
    {
        return getServer().getScheduler();
    }

    public void onEmenbeePluginEnable()
    {
    }

    public void onEmenbeePluginDisable()
    {
    }

    public void onEmenbeePluginLoad()
    {
    }

    public void log(Level level, String message)
    {
        logger.log(level, message);
    }

}
