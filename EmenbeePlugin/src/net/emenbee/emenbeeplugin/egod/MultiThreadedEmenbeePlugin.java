package net.emenbee.emenbeeplugin.egod;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class MultiThreadedEmenbeePlugin extends EmenbeePlugin
{
    private ThreadPoolExecutor threadPool;

    public MultiThreadedEmenbeePlugin(int amountOfThreads)
    {
        threadPool = ((ThreadPoolExecutor) Executors.newFixedThreadPool(amountOfThreads));
    }

    public void runOnThreadPool(Runnable task)
    {
        if (threadPool.getActiveCount() >= threadPool.getMaximumPoolSize()) {
            logger.warning("[EmenbeePlugin] Added task to thread pool while all threads are busy.");
        }
        threadPool.execute(task);
    }

    public <V>Future<V> runCallableOnThreadPool(Callable<V> callable)
    {
        if (threadPool.getActiveCount() >= threadPool.getMaximumPoolSize())
        {
            logger.warning("[EmenbeePlugin] Added task to thread pool while all threads are busy.");
        }
        return threadPool.submit(callable);
    }
}
